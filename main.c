#include <stdio.h>
#include <stdlib.h>
#include "tester.h"

int main(int argc, char* argv[])
{
    int szamlalo =  1;
    if(argv==NULL)
    {
        return 0;
    }
    while(argv[szamlalo] != NULL){
        tester(argv[szamlalo]);
        szamlalo++;
    }

    return 0;
}
