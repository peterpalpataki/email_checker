#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "tester.h"


void tester(char tomb[])
{
    int hossz = 0;
    int at_hely = -1;
    int dot_hely = -1;

    const char enged_karakterek[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    int nem_enged_kar_szam = 0;

    bool megfelelo = true;

    while(tomb[hossz] != '\0'){
        if (strchr(enged_karakterek, tomb[hossz]) == NULL) {
            nem_enged_kar_szam++;
        }
        if(tomb[hossz] == '@') {at_hely = hossz;}
        if(tomb[hossz] == '.') {dot_hely = hossz;}
        hossz++;
    }
    if(hossz < 8 || nem_enged_kar_szam != 2){
        megfelelo = false;
    }
    if(at_hely < 1 || at_hely == hossz){
        megfelelo = false;
    }
    if(dot_hely < 1 || dot_hely < at_hely || dot_hely == at_hely + 1 || (hossz - 4) < dot_hely){
        megfelelo = false;
    }

    if(megfelelo){
        printf("Az email cim megfelelo.\n");
    }else{
        printf("Az eamil cim nem megfelelo.\n");
    }
}
